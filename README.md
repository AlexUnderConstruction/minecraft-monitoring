# MineCraft Monitoring

This program is designed to parse MineCraft log file in real time and immediately notify specified Telegram Group for any changes you want to.

### Features

- Monitor Minecraft Server log file in realtime
- Send records from log file containing specified phrases or words in Telegram Group
- Handling rotation of log file

### Usage

There are two files needed to run this program:
- mcmon-0.9_64 (executable file)
- mcmon.conf (configuration file)

Put them in Minecraft Server folder or any other path you'd like.

You have to adjust mcmon.conf file according to your parameters and then run the program.
You can also specify which config file you would like to use with "-c" option (default is "mcmon.conf" in the same path), ex:
```
./mcmon-0.9_64 -c /home/mc/minecraft/mcmon.conf
```
Or you can generate new config sample file by running program with -create-config parameter:
```
./mcmon-0.9_64 -create-config /home/mc/minecraft/new-mcmon.conf
```
You have also put your Telegram Bot's token and Group's ID in config file. See next section for details.

In some cases you would like to run the program as a background process so it's not needed to hold console with this program opened.
Just install package "screen" for your Linux distro and run the program like this:
```
screen -S monitoring -d -m ./mcmon-0.9_64
```
You can now detach the screen by pressing [CTRL]+A D without killing the program.
If you would like to enter that screen again, just type in console (you have to be logged in with the same username):
```
screen -r monitoring
```
To show the list of all screen sessions with their names type in console:
```
screen -ls
```
Other useful commands and hotkeys of screen you could find in man pages or google it for most useful combinations. In most cases this should be enough: https://www.howtoforge.com/linux_screen

### Make Telegram Bot

1. Open https://t.me/botfather in your Telegram messenger or browser and create new Bot (that interface will guide you till the end). In result you will get Bot's token which looks like:
```
012345678:ABCd_aB1cD2eF3gH4iJ5kL6m_N7oP8qR9sT
```
2. Create new Group in Telegram and make your Bot to be member of it.

3. In a browser open the link https://api.telegram.org/bot<Bot_Token>/getUpdates . You will see some statistisc of your Bot in XML format. Just find the string where is your new Group's name and it's ID.

Bot's URL Example:
```
https://api.telegram.org/bot012345678:ABCd_aB1cD2eF3gH4iJ5kL6m_N7oP8qR9sT/getUpdates
```
Example of XML (the name of Group is "Minecraft Monitoring"):
```
{"id":-123456789,"title":"Minecraft Monitoring","type":"group","all_members_are_administrators":true}
```

4. Put Group's ID and Bot's Token in config file.
Example:
```
{
        "Logfile":              "latest.log",
        "Search_phrase":        ["the game", "ERROR", "FATAL", "Stopping server", "Starting minecraft server"],
        "Telegram":             true,
        "Bot_token":            "012345678:ABCd_aB1cD2eF3gH4iJ5kL6m_N7oP8qR9sT",
        "Chat_ID":              -123456789
}

```
P.S. Just in case. Group's ID could be changed by Telegram in some cases. For example, if you convert Group to Supergroup etc.
