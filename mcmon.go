package main

import (
	"fmt"
	"net/http"
	"bytes"
	"encoding/json"
	"os"
	"strings"
	"time"
//	"strconv"
	"log"
	"flag"
	"bufio"

	"github.com/hpcloud/tail"
)

var (
	verNumber string = "0.9"
	//searchPhrase = []string{"the game", "ERROR", "FATAL", "Stopping server", "Starting minecraft server"}
	//cfgFile string = "mcmon.conf"
	//useTelegram bool = false
	botToken string = "012345678:ABCd_aB1cD2eF3gH4iJ5kL6m_N7oP8qR9sT"
	chatID int64 = -123456789
	flgVersion bool
	newConfig string
	overwrite bool
//	newCfgFile stringFlag

)

type cfgJson struct {
	Logfile string
	Search_phrase []string
	Telegram bool
	Bot_token string
	Chat_ID int64
}

type sendMessageReqBody struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

func notifyTelegram(message string) error {
	reqBody := &sendMessageReqBody{
		ChatID: chatID,
		Text:   message,
	}
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		fmt.Println(err)
		return err
	}
	resp, err := http.Post(
		"https://api.telegram.org/bot"+botToken+"/"+"sendMessage",
		"application/json",
		bytes.NewBuffer(reqBytes),
	)
	if err != nil {
		fmt.Println(err)
		return err
	}
	_ = resp
return err
}

func fileExists(filename string) bool {
    info, err := os.Stat(filename)
    if os.IsNotExist(err) {
        return false
    }
    return !info.IsDir()
}

//func init() {
//	flag.Var(&newCfgFile, "mcmon.conf", "Crete new config file example")
//}

func main() {
	fmt.Println("MineCraft Monitoring [MC-Mon] v." + verNumber + ".\n")
	cfgFile := flag.String("c", "mcmon.conf", "Specify configuration file which MC-Mon will use.")
	flag.StringVar(&newConfig, "create-config", "", "Specify the name of new configuration example file.")
	//createConfigFile := flag.String("c", "mcmon.conf", "Specify the name of new configuration-example file.")
	flag.BoolVar(&flgVersion, "v", false, "Print version and exit.")
//	newCfg := flag.String("create-config", mcmon.conf, "Create new config file example.")
//	flag.BoolVar(&flgCreateConfig, "create-config", false, "Create new config file example")
	flag.Parse()

	if flgVersion {
		os.Exit(0)
	}

	if newConfig != "" {
		if fileExists(newConfig) {
			reader := bufio.NewReader(os.Stdin)
			for {
				fmt.Printf("File %v already exists. Would you like to overwrite it? [y/n]: ", newConfig)
				response, err := reader.ReadString('\n')
				if err != nil {
					log.Fatal(err)
				}
				response = strings.ToLower(strings.TrimSpace(response))
				if response == "y" || response == "yes" {
					overwrite = true
					break
				} else if response == "n" || response == "no" {
					//overwrite = false
					os.Exit(0)
				}
			}
			} else {
				overwrite = true
			}

		if overwrite {
			fmt.Printf("Generating new config file %v ...\n", newConfig)
			ncfile, err := os.OpenFile(newConfig, os.O_CREATE|os.O_WRONLY, 0644)
			defer ncfile.Close()
			if err != nil {
				log.Fatal("Cannot create file", err)
			} else {
				d := []string{"{", "\t\"Logfile\":\t\t\"logs/latest.log\",", "\t\"Search_phrase\":\t[\"the game\", \"ERROR\", \"FATAL\", \"Stopping server\", \"Starting minecraft server\"],", "\t\"Telegram\":\t\ttrue,", "\t\"Bot_token\":\t\t\"012345678:ABCd_aB1cD2eF3gH4iJ5kL6m_N7oP8qR9sT\",", "\t\"Chat_ID\":\t\t-123456789", "}"}
				for _, v := range d {
					fmt.Fprintln(ncfile, v)
					if err != nil {
						fmt.Println(err)
						return
					} else {
					}
				}
				fmt.Printf("Done.\n")
			}
			os.Exit(0)
		}

		os.Exit(0)
	}

	file, err := os.Open(*cfgFile)
	if err != nil {
		fmt.Println("[MC-Mon]: ERROR: Can not read config file \"" + *cfgFile + "\".")
		log.Fatal(err)
//		os.Exit(1)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := cfgJson{}
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("[MC-Mon]: ERROR: Config file " + *cfgFile + " is not valid. You have to fix it or create a new one with \"mcmon -create-config mcmon.conf\" command. ", err)
		os.Exit(1)
	}

	path, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Start parameters:\t", os.Args)
	fmt.Println("Current path:\t\t", path)
	fmt.Println("Configuration file:\t", *cfgFile)
	logFile := configuration.Logfile
	fmt.Println("Minecraft log file:\t", logFile)
	searchPhrase := configuration.Search_phrase
	fmt.Println("Search phrases:\t\t", strings.Join(searchPhrase, " | "))
	fmt.Println()

	useTelegram := configuration.Telegram
	fmt.Println("Telegram notifications:\t", useTelegram)
	if useTelegram {
		botToken = configuration.Bot_token
		fmt.Println("Bot token:\t\t", botToken)
		chatID = configuration.Chat_ID
		fmt.Println("Chat ID:\t\t", chatID)
	}
	fmt.Println()

	t, err := tail.TailFile(logFile, tail.Config{
                Follow: true,
                ReOpen: true,
                Location: &tail.SeekInfo{0, os.SEEK_END}})
        if err != nil {
                fmt.Println(err)
        }
	fmt.Println("[MC-Mon]: Started at " +  time.Now().Format("2006-01-02 15:04:05") + ".\n")
        for line := range t.Lines {
                fmt.Println(line.Text)
		for i, matchString := range searchPhrase {
			if strings.Contains(line.Text, matchString) {
				fmt.Println("[MC-Mon] [Match found]:", line.Text)
				_ = i
				if useTelegram {
					sendToTelegram := notifyTelegram(line.Text)
					if sendToTelegram != nil {
						fmt.Println(sendToTelegram)
					}
				}
				break
			}
		}
        }

}


